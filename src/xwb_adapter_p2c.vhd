-------------------------------------------------------------------------------
-- Title      :
-- Project    : Wishbone Wishbone
-------------------------------------------------------------------------------
-- File       : xwb_adapter_p2c.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-10-25
-- Last update: 2020-10-25
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2020 Piotr Miedzik
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library general_cores;
use general_cores.wishbone_pkg.all;

entity xwb_adapter_p2c is
  port (
    clk_sys_i : in std_logic;
    rst_n_i   : in std_logic;

    slave_i    : in  t_wishbone_slave_in                                   := cc_dummy_slave_in;
    slave_o : out t_wishbone_slave_out;

    master_i   : in  t_wishbone_master_in                                  := cc_dummy_slave_out;
    master_o : out t_wishbone_master_out
    );
end xwb_adapter_p2c;

architecture rtl of xwb_adapter_p2c is


--  signal master_out : t_wishbone_master_out;
  signal master_out_adr: std_logic_vector(t_wishbone_master_out.adr'range);
  
  
  signal slave_in_cyc_and_stb : std_logic := '0';
    
  signal slave_in_dat:std_logic_vector(31 downto 0);
  signal slave_in_adr:std_logic_vector(31 downto 0);
  signal slave_in_sel:std_logic_vector(3 downto 0);
  signal slave_in_we:std_logic;
  signal slave_in_stb:std_logic;
begin

    p_register: process (clk_sys_i) is
    begin
      if rising_edge(clk_sys_i) then       
        if slave_in_cyc_and_stb = '0' then
          slave_in_adr <= slave_i.adr;
          slave_in_dat <= slave_i.dat;
          slave_in_sel <= slave_i.sel;
          slave_in_we <= slave_i.we;
          slave_in_stb <= slave_i.stb;
        end if;
                
        if slave_i.cyc = '0' then
          slave_in_cyc_and_stb <=  '0';
        elsif (master_i.ack = '1' or master_i.err = '1' or  master_i.rty = '1') and slave_in_cyc_and_stb = '1' then
          slave_in_cyc_and_stb <=  '0';
        elsif slave_i.cyc = '1' and slave_i.stb = '1' then
          slave_in_cyc_and_stb <=  '1';
        end if;
        
        if rst_n_i = '0' then
          slave_in_cyc_and_stb <=  '0';
        end if;
      end if;
    end process p_register;

    slave_o.stall <= slave_in_cyc_and_stb;
    slave_o.ack   <= master_i.ack when slave_in_cyc_and_stb='1' else '0';
    slave_o.err   <= master_i.err when slave_in_cyc_and_stb='1' else '0';
    slave_o.rty   <= master_i.rty when slave_in_cyc_and_stb='1' else '0';
    slave_o.dat   <= master_i.dat;

    
    master_o.cyc <= slave_i.cyc;    
    master_o.adr <= slave_in_adr when slave_in_cyc_and_stb='1' else (others => '0');
    master_o.dat <= slave_in_dat when slave_in_cyc_and_stb='1' else (others => '0');
    master_o.sel <= slave_in_sel when slave_in_cyc_and_stb='1' else (others => '0');
    master_o.we  <= slave_in_we  when slave_in_cyc_and_stb='1' else '0';
    master_o.stb <= slave_in_stb when slave_in_cyc_and_stb='1' else '0';
  
end rtl;
