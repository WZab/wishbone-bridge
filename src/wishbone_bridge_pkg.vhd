-------------------------------------------------------------------------------
-- Title      :
-- Project    : Wishbone Wishbone
-------------------------------------------------------------------------------
-- File       : wishbone_bridge_pkg.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-10-25
-- Last update: 2020-10-25
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2020 Piotr Miedzik
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library general_cores;
use general_cores.wishbone_pkg.all;

package wishbone_bridge_pkg is

  component xwb_wb_wb is
  generic(
    -- Address of the slaves connected
    g_zeropage_address     : t_wishbone_address_array;
    g_zeropage_mask        : t_wishbone_address_array;
    -- output interface mode
    g_wishbone_mode        : t_wishbone_interface_mode      := PIPELINED;
    g_wishbone_granularity : t_wishbone_address_granularity := BYTE;
    g_timeout: boolean := true);
  port(
    clk_sys_i     : in  std_logic;
    rst_n_i       : in  std_logic;
    -- slave connection from ... , PIPELINED
    s_input_m2s: in  t_wishbone_slave_in;
    s_input_s2m: out t_wishbone_slave_out;
    -- Master connection to the Wishbone slaves
    m_output_m2s: out t_wishbone_master_out;
    m_output_s2m: in  t_wishbone_master_in;
    -- additional zero page, PIPELINED 
    m_zeropage_m2s: out t_wishbone_master_out_array(g_zeropage_address'length -1 downto 0);
    m_zeropage_s2m: in  t_wishbone_master_in_array(g_zeropage_address'length -1 downto 0)
  );
end component xwb_wb_wb;

component xwb_timeout is
  generic (
    g_counter_width: natural := 8
  );
  port(
    clk_i     : in  std_logic;
    rst_n_i       : in  std_logic;
    
    s_input_m2s: in  t_wishbone_slave_in;
    s_input_s2m: out t_wishbone_slave_out;
    -- Master connection to the Wishbone slaves
    m_output_m2s: out t_wishbone_master_out;
    m_output_s2m: in  t_wishbone_master_in;
    -- additional zero page 
    timeout_counter_i: in std_logic_vector(g_counter_width-1 downto 0)
  );
end component xwb_timeout;

 component xwb_crossbar_fallback is
  generic(
    g_num_masters : integer := 2;
    g_num_slaves  : integer := 1;
    g_registered  : boolean := false;
    -- Address of the slaves connected
    g_address     : t_wishbone_address_array;
    g_mask        : t_wishbone_address_array;
   -- Set to false to skip "Mapping Slave" notes during simulation
    g_verbose     : boolean := true);
  port(
    clk_sys_i     : in  std_logic;
    rst_n_i       : in  std_logic;
    -- Master connections (INTERCON is a slave)
    slave_i       : in  t_wishbone_slave_in_array(g_num_masters-1 downto 0);
    slave_o       : out t_wishbone_slave_out_array(g_num_masters-1 downto 0);
    -- Slave connections (INTERCON is a master)
    master_i      : in  t_wishbone_master_in_array(g_num_slaves-1 downto 0);
    master_o      : out t_wishbone_master_out_array(g_num_slaves-1 downto 0);
    -- fallback connectios (INTERCON is a master)
    fallback_i      : in  t_wishbone_master_in;
    fallback_o      : out t_wishbone_master_out;
    -- Master granted access to SDB for use by MSI crossbar (please ignore it)
    sdb_sel_o     : out std_logic_vector(g_num_masters-1 downto 0));
  end component xwb_crossbar_fallback;
  
  
  
  component xwb_adapter is

  generic (
    g_master_mode        : t_wishbone_interface_mode;
    g_master_granularity : t_wishbone_address_granularity;
    g_slave_mode         : t_wishbone_interface_mode;
    g_slave_granularity  : t_wishbone_address_granularity
    );
  port (
    clk_sys_i : in std_logic;
    rst_n_i   : in std_logic;

    slave_i    : in  t_wishbone_slave_in                                   := cc_dummy_slave_in;
    slave_o : out t_wishbone_slave_out;

    master_i   : in  t_wishbone_master_in                                  := cc_dummy_slave_out;
    master_o : out t_wishbone_master_out
    );
end component xwb_adapter;
  
component xwb_adapter_p2c is
  port (
    clk_sys_i : in std_logic;
    rst_n_i   : in std_logic;

    slave_i    : in  t_wishbone_slave_in                                   := cc_dummy_slave_in;
    slave_o : out t_wishbone_slave_out;

    master_i   : in  t_wishbone_master_in                                  := cc_dummy_slave_out;
    master_o : out t_wishbone_master_out
    );
end component xwb_adapter_p2c;

  component xwb_bridge is
  generic(
    g_counter_width: natural := 8;
    g_default_timeout: natural := 64

  );
  port(
    clk_i     : in  std_logic;
    rst_n_i       : in  std_logic;
    
    s_input_m2s: in  t_wishbone_slave_in;
    s_input_s2m: out t_wishbone_slave_out;
    -- Master connection to the Wishbone slaves
    m_output_m2s: out t_wishbone_master_out;
    m_output_s2m: in  t_wishbone_master_in;
    -- additional zero page 
    s_ctl_m2s: in  t_wishbone_slave_in;
    s_ctl_s2m: out t_wishbone_slave_out;
    
    counter_o: out std_logic_vector(g_counter_width-1 downto 0)
  );
  end component xwb_bridge;
end wishbone_bridge_pkg;

package body wishbone_bridge_pkg is

end wishbone_bridge_pkg;
